import instance from "@/config/axios";

export async function getProduit() {
    try {
        const response = await instance.get('/produit');
        return response.data;
    } catch (ex) {
        console.log(ex)
    }
}

export async function getCategorie() {
    try {
        const response = await instance.get('/categorie');
        return response.data;
    } catch (ex) {
        console.log(ex)
    }
}

export async function addUser(form) {
    try {
        const response = await instance.post('/user', form);
        console.log(response);
        return response.data;
    } catch (ex) {
        console.log(ex)
    }
}

export async function loginUser(credentials) {
    try {
        const response = await instance.post('/user', credentials);
        return response.data;
    } catch (ex) {
        console.log(ex);
        throw ex;
    }
}


export function isAuthenticated() {
    return !!localStorage.getItem('AUTH_TOKEN');
}