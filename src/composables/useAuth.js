
import { useRouter } from 'vue-router';
import { ref } from 'vue';

const user = ref(null);

export function useAuth() {

    const router = useRouter()

    function logout() {
        localStorage.clear();
        router.push({ name: 'login' });
    }

    return {
        logout,

    }
}