import { ref } from 'vue';
import { getProduit } from '../service/api.service';

export function useApi() {

    const produits = ref([]);

    const fetchProduit = async () => {
        try {
            const response = await getProduit();

            produits.value = response;

            console.log("liste des produits : " + produits.value);

        } catch (error) {
            console.error('Erreur lors de la récupération des menus dans App.vue :', error);
        }
    }

    return {
        fetchProduit,
        produits
    }
}