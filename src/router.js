import { createRouter, createWebHashHistory } from 'vue-router'
import Home from "./components/pages/Home.vue"
import Login from "./components/pages/Login.vue"
import SignIn from "./components/pages/SignIn.vue"

export const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/logout',
            name: 'logout'
        },
        {
            path: '/signIn',
            name: 'signIn',
            component: SignIn
        },
    ]
});